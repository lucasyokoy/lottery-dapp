// const assert = require('assert');
// const ganache = require('ganache-cli');
// const Web3 = require('web3');
// const web3 = new Web3(ganache.provider());

// const { interface, bytecode } = require('../compile');

// let lottery;
// let accounts;

// beforeEach(async () => {
//     accounts = await web3.eth.getAccounts();
//     lottery = await new web3.eth.Contract(JSON.parse(interface)
//         .deploy({ data: bytecode }).send({ from: accounts[0], gas: '1000000' }));
// });

// describe('lottery contract', () => {
//     it('deploys a contract', () => {
//         asssert.ok(lottery.options.address);
//     });
//     it('allows one account to enter', () => {
//         await lottery.methods.enter().send({
//             from: accounts[0],
//             value: web3.utils.toWei('0.02', 'ether')
//         });
//     });
//     const players = await lotery.methods.getPlayers().call({ from: accounts[0] });
//     assert.equal(accounts[0], players[0]);
//     assert.equal(1, players.length, 'the only player should be the one that sent the transaction above');
// });

const _deploy_contracts = require("../migrations/1_initial_migration");

var Lottery = artifacts.require("./Lottery.sol");

contract('Lottery', function (accounts) {
    var player1 = accounts[1];
    var player2 = accounts[2];
    var correctBet = 10000000000000000;
    var wrongBet1 = 10000;
    var wrongBet2 = 100000000000000000;
    it('initializes the contract with the correct values', function () {
        return Lottery.deployed().then(function (instance) {
            lotteryInstance = instance;
            return lotteryInstance.manager();
        }).then(function (manager) {
            assert.equal(manager, accounts[0], 'the manager was properly established');
            return lotteryInstance.getAllPlayers();
        }).then(function (players) {
            assert.equal(players.length, 0, 'there should be no players immediatelly after deployment');
        });    
    });

    it('attempts to join the game', function () {
        return Lottery.deployed().then(function (instance) {
            lotteryInstance = instance;
            return lotteryInstance.enter({ from: player1, value: correctBet });
        }).then(function (receipt) {
            return lotteryInstance.getAllPlayers();
        }).then(function (players) {
            assert.equal(players.length, 1, 'one new player joined the game');
            return lotteryInstance.enter({ from: player2, value: wrongBet1 });
        }).then(assert.fail).catch(function (error) {
            //there should be an error if you tried sending the wrong bet
            return lotteryInstance.enter({ from: player2, value: wrongBet2 });
        }).then(assert.fail).catch(function (error) {
            //there should be an error if you tried sending the wrong bet
        });
    });

    it('pick a winner', function () {
        return Lottery.deployed().then(function (instance) {
            lotteryInstance = instance;
            lotteryInstance.enter({ from: player1, value: correctBet });
            lotteryInstance.enter({ from: player2, value: correctBet });
            return lotteryInstance.pickWinner({from: player1});
        }).then(assert.fail).catch(function(error){
            assert(error.message.indexOf('revert') >= 0,'no one except the admin should be allowed to call pickWinner');
            lotteryInstance.pickWinner({from: accounts[0]});
            return lotteryInstance.getAllPlayers();
        }).then(function(players){
            assert.equal(players.length,0,'list of players should have been reset, after the manager has called pickWinner');
        });
    });
});

// contract('DappTokenSale', function (accounts) {
//     var tokenSaleInstance;
//     var tokenPrice = 1000000000000000; //in wei
//     var tokensAvailable = 750000; //75% of the 1M total
//     var admin = accounts[0];
//     var buyer = accounts[1];
//     var numberOfTokens = 10;

//     it('initializes the contract with the correct values', function () {
//         return DappTokenSale.deployed().then(function (instance) {
//             tokenSaleInstance = instance;
//             return tokenSaleInstance.address;
//         }).then(function (address) {
//             assert.notEqual(address, 0x0, 'has a non null address');
//             return tokenSaleInstance.tokenContract();
//         }).then(function (contractAddress) {
//             assert.notEqual(contractAddress, 0x0, 'has token contract address (is linked to DappToken.sol)');
//             return tokenSaleInstance.tokenPrice();
//         }).then(function (price) {
//             assert.equal(price, tokenPrice, 'token price is correct');
//         });
//     });

//     it('facilitates token buying', function () {
//         return DappToken.deployed().then(function (instance) {
//             //grab token instance first
//             tokenInstance = instance;
//             return DappTokenSale.deployed();
//         }).then(function (instance) {
//             //grab token sales instance
//             tokenSaleInstance = instance;
//             //provision 75% of all tokens to the DappTokenSale contract
//             return tokenInstance.transfer(tokenSaleInstance.address,tokensAvailable,{from: admin});
//         }).then(function(receipt){
//             numberOfTokens = 10;
//             return tokenSaleInstance.buyTokens(numberOfTokens, {from: buyer, value: numberOfTokens * tokenPrice})
//         }).then(function (receipt) {
//             assert.equal(receipt.logs.length, 1, 'triggers one event');
//             assert.equal(receipt.logs[0].event, 'Sell', 'the triggered event should be a Sell event');
//             assert.equal(receipt.logs[0].args._buyer, buyer, 'logs the account the tokens are transfered to');
//             assert.equal(receipt.logs[0].args._amount, numberOfTokens, 'logs the transfered amount');
//             return tokenSaleInstance.tokenSold();
//         }).then(function (amount) {
//             assert.equal(amount.toNumber(), numberOfTokens, 'increments the number of tokens sold');
//             return tokenInstance.balanceOf(buyer)
//         }).then(function(balance){
//             assert.equal(balance.toNumber(), numberOfTokens, 'buyer should have its balance incremented')
//             return tokenInstance.balanceOf(tokenSaleInstance.address)
//         }).then(function(balance){
//             assert.equal(balance.toNumber(),tokensAvailable - numberOfTokens,'contract should be left with the remaining tokens after the purchase');
//             return tokenSaleInstance.buyTokens(numberOfTokens, { from: buyer, value: 1 });
//         }).then(assert.fail).catch(function (error) {
//             assert(error.message.indexOf('revert') >= 0, 'msg.value must equal the number of tokens in wei');
//             return tokenSaleInstance.buyTokens(800000, {from: buyer, value: numberOfTokens * tokenPrice})
//         }).then(assert.fail).catch(function (error) {
//             assert(error.message.indexOf('revert') >= 0, 'cant buy more tokens than there are provisioned');
//         });
//     });

//     it('ends token sale',function(){
//         return DappToken.deployed().then(function (instance) {
//             //grab token instance first
//             tokenInstance = instance;
//             return DappTokenSale.deployed();
//         }).then(function (instance) {
//             //grab token sales instance
//             tokenSaleInstance = instance;
//             //try to end sale from account other than admin
//             return tokenSaleInstance.endSale({from: buyer});
//         }).then(assert.fail).catch(function(error){
//             assert(error.message.indexOf('revert') >= 0,'sale can only be ended by admin');
//             //try to end sale as admin
//             return tokenSaleInstance.endSale({from:admin});
//         }).then(function(receipt){
//             //no assert is required, as, for now, we just want to know if there was any error here
//             return tokenInstance.balanceOf(admin);
//         }).then(function(balance){
//             assert.equal(balance.toNumber(),999990,'returns all unsold tokens to admin');
//             return tokenSaleInstance.tokenPrice();
//         }).then(assert.fail).catch(function(error){
//             //just make sure there is an error
//             // if the contract was indeed destroyed, there won't be a price to fetch
//         });
//     });