// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.8.0;

contract Lottery {
  address public manager;
  address payable [] public players;
  uint256 public price;
  //the getter function automatically made by solidity for public variables only returns one individual item on the array at a time.
  //this function instead returns the entire array
  function getAllPlayers() public view returns(address payable [] memory){
    return players;
  }
  
  constructor() {
    manager = msg.sender;
    price = 0.01 ether;
  }

  function enter() payable public{
    require(msg.value == price,'Wrong value sent. Please send 0.01 eth');
    players.push(msg.sender);
  }

  function randomNumberGenerator() private view returns (uint256){
    //DO NOT USE THE TIMESTAMP AS A SOURCE OF RANDOMNESS
    //abi.encode is needed to turn the arguments into an appropriate input for keccak256
    //keep in mind that keccak256 is an instance of SHA3
    return uint256(keccak256(abi.encode(block.timestamp,block.difficulty,players)));
  }

  //the modifier can be added to any function, and will be executed before the rest of the function
  //it can also receive arguments
  modifier restrictedToManager(){
    require(msg.sender == manager,'Only the manager of the lottery can call this function');
    //the _; represents the rest of the code being executed.
    _;
    //more code from the modifier can be put after the _; and it'll be executed after the function's code
  }

  function pickWinner() public restrictedToManager{
    //the modulo function picks the winner regardless of the relative size between the random number and the players array
    //this is generally a good way to pick a random element out of a list
    uint index = randomNumberGenerator() % players.length;
    //sends all of the funds in the contract to the winner
    players[index].transfer(address(this).balance);
    //resets the players list with a new dynamic list starting with a length 0
    players = new address payable[](0);
  }
}
